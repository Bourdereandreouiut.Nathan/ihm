import javafx.event.EventHandler;
import javafx.event.ActionEvent;
public class ControleurBoutonConvertirF implements EventHandler<ActionEvent>{
    private AppliConverter appli;
    private Temperature temp;
    
    public ControleurBoutonConvertirF(AppliConverter appli,Temperature temp){
        this.appli = appli;
        this.temp = temp;
    }
    @Override
    public void handle(ActionEvent event){
        double valc;
        try{
        valc = this.appli.getValueFahrenheit();
        this.temp.setvaleurFahrenheit(valc);
        this.appli.majTF();
        }
        catch (NumberFormatException exc){
            this.appli.effaceTF();
        }
    }
}