import javafx.event.EventHandler;
import javafx.event.ActionEvent;
public class ControleurBoutonConvertirK implements EventHandler<ActionEvent>{
    private AppliConverter appli;
    private Temperature temp;
    
    public ControleurBoutonConvertirK(AppliConverter appli,Temperature temp){
        this.appli = appli;
        this.temp = temp;
    }
    @Override
    public void handle(ActionEvent event){
        double valc;
        try{
        valc = this.appli.getValueKelvin();
        this.temp.setvaleurKelvin(valc);
        this.appli.majTF();
        }
        catch (NumberFormatException exc){
            this.appli.effaceTF();
        }
    }
}