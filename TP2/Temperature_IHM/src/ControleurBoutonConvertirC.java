import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurBoutonConvertirC implements EventHandler<ActionEvent>{
    private AppliConverter appli;
    private Temperature temp;
    
    public ControleurBoutonConvertirC(AppliConverter appli,Temperature temp){
        this.appli = appli;
        this.temp = temp;
    }
    @Override
    public void handle(ActionEvent event){
        double valc;
        try{
        valc = this.appli.getValueCelcius();
        this.temp.setvaleurCelcius(valc);
        this.appli.majTF();
        }
        catch (NumberFormatException exc){
            this.appli.effaceTF();
        }
    }
}