import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;

public class ControleurOperation implements EventHandler<ActionEvent>{
    private AppliCalculatrice app;
    private Calculatrice calc;

    public ControleurOperation(AppliCalculatrice app,Calculatrice calc){
        this.app = app;
        this.calc = calc;
    }
    @Override
    public void handle(ActionEvent event){
        try{
            Button b = (Button)event.getTarget();
            this.calc.setVal1(this.app.getValueTF1());
            this.calc.setVal2(this.app.getValueTF2());
            String t = b.getText();
            if (t=="+"){
                this.app.setOperation(t);
            }
            else if (t=="-"){
                this.app.setOperation(t);
            }
            else if (t=="*"){
                this.app.setOperation(t);
            }
            else if (t=="/"){
                this.app.setOperation(t);
            }
            else if (t=="="){
                this.app.majres();
                this.app.majHistorique();
            }
            
        }
        catch (NumberFormatException e){
            this.app.resetAll();
        }
    }
    }
