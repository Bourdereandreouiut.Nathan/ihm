import javafx.application.Application;
import javafx.application.Platform;
// import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.Slider;
import javafx.scene.layout.GridPane;

public class AppliCalculatrice extends Application{
    protected TextField op1;
    protected TextField op2;
    protected Label lres;
    protected TextArea historique;
    private Calculatrice calculatrice;
    private String operation;
    
    @Override 
    public void init(){
        this.calculatrice = new Calculatrice();
    }
    @Override
    public void start(Stage stage) throws Exception{
        // Construction du graphe de scène
        VBox root = new VBox();
        
        this.ajouteTF1(root);
        this.ajouteTF2(root);
        this.ajouteRes(root);
        this.ajouteBoutons(root);
        this.ajouteHistorique(root);
        
        Scene scene = new Scene(root);
        stage.setTitle("Calculatrice");
        stage.setScene(scene);
        stage.show();
    }
    public double getValueTF1() throws NumberFormatException{
        return Double.parseDouble(op1.getText());
    }
    public double getValueTF2() throws NumberFormatException{
        return Double.parseDouble(op2.getText());
    }
    public void resetAll(){
        this.op1.setText("");
        this.op2.setText("");
        this.lres.setText("");
    }
    public void quitte(){
        Platform.exit();
    }
    public void setOperation(String operation){
        this.operation = operation;
    }
    public String getOperation(){
        return this.operation;
    }
    public void majres(){
        
            if (this.operation=="+"){
            this.lres.setText(this.calculatrice.additionner()+"");
            }
            else if (this.operation=="-"){
            this.lres.setText(this.calculatrice.soustraire()+"");
            }
            else if (this.operation=="*"){
            this.lres.setText(this.calculatrice.multiplier()+"");
            }
            else if (this.operation=="/"){
            this.lres.setText(this.calculatrice.diviser()+"");
            }
        }
    public void majHistorique(){
        this.historique.setEditable(true);
        this.historique.appendText("oui\n");
        //truc a faire entre deux
        this.historique.setEditable(false);
    }
    public void cleanHistorique(){
        this.historique.clear();
    }
    private void ajouteTF1(VBox root){
    
        HBox pane = new HBox(10);     
        pane.setPadding(new Insets(5,5,0,0));
        this.op1 = new TextField();
        pane.getChildren().addAll(this.op1);  
        pane.setAlignment(Pos.CENTER);  
        root.getChildren().add(pane);
    }
    private void ajouteTF2(VBox root){
        HBox pane = new HBox(10);
        pane.setPadding(new Insets(5,5,0,0));
        this.op2 = new TextField();
        pane.getChildren().addAll(this.op2);
        pane.setAlignment(Pos.CENTER);
        root.getChildren().add(pane);
    }
    private void ajouteRes(VBox root){
        HBox pane = new HBox(20);
        pane.setPadding(new Insets(10,10,10,10));
        Label resultat = new Label("Résultat : ");
        this.lres = new Label("");
        pane.getChildren().addAll(resultat,this.lres);
        root.getChildren().add(pane);
    }
    private void ajouteBoutons(VBox root){
        HBox pane =new HBox(50);
        Label lab = new Label("Opérations");
        pane.setPadding(new Insets(10,0,0,0));
        Button boutona = new Button("+");
        Button boutonb = new Button("-");
        Button boutonp =new Button("*");
        Button boutonm =new Button("/");
        Button equals = new Button("=");
        //equals.setPadding(new Insets(0,0,10,10));
        lab.setAlignment(Pos.TOP_LEFT);              
        pane.getChildren().addAll(boutona,boutonb,boutonp,boutonm,equals); 
        boutonp.setOnAction(new ControleurOperation(this,this.calculatrice));
        boutonm.setOnAction(new ControleurOperation(this,this.calculatrice));
        boutona.setOnAction(new ControleurOperation(this,this.calculatrice));
        boutonb.setOnAction(new ControleurOperation(this,this.calculatrice));
        equals.setOnAction(new ControleurOperation(this,this.calculatrice));
        pane.setAlignment(Pos.CENTER);
        equals.setAlignment(Pos.BOTTOM_RIGHT);
        root.getChildren().add(pane);
        root.getChildren().add(equals);
    }
    private void ajouteHistorique(VBox root){
        GridPane v = new GridPane();
        Label lab = new Label("Historique des Opérations");
        Button b = new Button("Effacer");
        this.historique = new TextArea();
        this.historique.setEditable(false);
        this.historique.setMaxWidth(300);
        this.historique.setMaxHeight(300);
        v.add(lab,1,0);
        v.add(this.historique,1,1);
        v.setAlignment(Pos.BOTTOM_CENTER);
        root.getChildren().add(v);
    }
    public static void main(String [] args){
        launch(args);
    }
}
