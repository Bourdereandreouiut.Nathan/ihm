public class Calculatrice{
    private double val1;
    private double val2;
    public Calculatrice(double val1,double val2){
        this.val1 = val1;
        this.val2 = val2;
    }
    public Calculatrice(){
        this.val1 = 0.0;
        this.val2 = 0.0;
    }
    public void setVal1(double val){
        this.val1 = val;
    }
    public void setVal2(double val){
        this.val2 = val;
    }
    public double additionner() throws NumberFormatException{
        try{
        return this.val1 + this.val2;
        }
        catch (NumberFormatException e){
            return 0.0;
        }
    }
    public double soustraire() throws NumberFormatException{
        try{
            return this.val1 - this.val2;
        }
        catch (NumberFormatException e){
            return 0.0;
        }
    }
    public double diviser() throws NumberFormatException{
        try{
            return this.val1 / this.val2;
        }
        catch (NumberFormatException e){
            return 0.0;
        }
    }
    public double multiplier() throws NumberFormatException{
        try{
            return this.val1 * this.val2;
        }
        catch (NumberFormatException e){
            return 0.0;
        }
    }
}