import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurBouton implements EventHandler<ActionEvent>{
    private DeuxFenetres appli;
    
    public ControleurBouton(DeuxFenetres appli){
        this.appli = appli;
    }
    
    @Override
    public void handle(ActionEvent event){
        Button button = (Button)(event.getSource());
        if (button.getText().contains("Connexion"))
            this.appli.afficheFenetreAnalyse();
        else if(button.getText().contains("Déconnexion"))
            this.appli.afficheFenetreConnexion();
    }
}
