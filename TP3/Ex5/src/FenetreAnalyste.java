// import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.scene.chart.PieChart;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.TilePane;
import javafx.scene.image.ImageView;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.geometry.*;

public class FenetreAnalyste extends BorderPane{
        private Button bouton;

        public FenetreAnalyste(Button b){
        super();
        this.bouton = b;

        this.setTop(this.ajouteTopBorderPane());
        this.setCenter(ajouteCenterVBox());
        this.setRight(ajouteRightTilePane());

    }

    public BorderPane ajouteTopBorderPane(){
        BorderPane top = new BorderPane();
        top.setPadding(new Insets(10));
        Text txt = new Text("Module Analyste");
        txt.setFont(Font.font(" Arial ",FontWeight.BOLD,32));
        top.setLeft(txt);
        top.setRight(this.bouton);
        top.setStyle("-fx-background-color:rgb(46,82,180);");
        return top;
    }

    public VBox ajouteCenterVBox(){
        VBox center = new VBox();
        center.setPadding(new Insets(10));
        Text texte = new Text("Analyse");
        ComboBox<String> cb = new ComboBox<String>();
        cb.getItems().addAll("Pie","Line","Bar");
        cb.setValue("Pie");
        PieChart chart = new PieChart();
        chart.setTitle("Que lisez-vous au petit déjeuner ?");
        chart.getData().setAll(
        new PieChart.Data("Le journal", 21),
        new PieChart.Data("Un livre", 3),
        new PieChart.Data("Le courrier", 7),
        new PieChart.Data("La boîte de céréales", 75));
        chart.setLegendSide(Side.LEFT);
        HBox hb = new HBox();
        Button buttonhb1 = new Button("Question Précédente",new ImageView("back.png"));
        Button buttonhb2 = new Button("Question Suivante",new ImageView("next.png"));
        hb.getChildren().addAll(buttonhb1,buttonhb2);
        hb.setPadding(new Insets(10));
        center.getChildren().addAll(texte,cb,chart,hb);
        return center;
    }
    public TilePane ajouteRightTilePane(){
        TilePane right = new TilePane();
        ImageView img1 = new ImageView("chart_1.png");
        ImageView img2 = new ImageView("chart_2.png");
        ImageView img3 = new ImageView("chart_3.png");
        ImageView img4 = new ImageView("chart_4.png");
        ImageView img5 = new ImageView("chart_5.png");
        ImageView img6 = new ImageView("chart_6.png");
        ImageView img7 = new ImageView("chart_7.png");
        ImageView img8 = new ImageView("chart_8.png");
        right.setPadding(new Insets(10));
        right.setStyle("-fx-background-color:rgb(135,206,250);");
        right.setMinSize(6, 2);
        right.getChildren().addAll(img1,img2,img3,img4,img5,img6,img7,img8);
        return right;
    }
}