import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.scene.image.ImageView;

public class DeuxFenetres extends Application{
    private Button btnConnexion;
    private Button btnDeconnexion;
    private Scene scene;
    @Override
    public void init(){
        this.btnConnexion = new Button("Connexion");
        this.btnDeconnexion = new Button("Déconnexion",new ImageView("user.png"));

        ControleurBouton control = new ControleurBouton(this);
        this.btnConnexion.setOnAction(control);
        this.btnDeconnexion.setOnAction(control);
    }
    @Override
    public void start(Stage stage) throws Exception{
        Pane root = new FenetreConnexion(this.btnConnexion);
        this.scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Allo45");
        stage.show();
    }
    public void afficheFenetreConnexion(){
        Pane root = new FenetreConnexion(this.btnConnexion);
        this.scene.setRoot(root);
        root.getScene().getWindow().sizeToScene();
    }
    public void afficheFenetreAnalyse(){
        Pane root = new FenetreAnalyste(this.btnDeconnexion);
        this.scene.setRoot(root);
        root.getScene().getWindow().sizeToScene();
    }
    public static void main(String[] args){
        launch(DeuxFenetres.class,args);
    }
}

