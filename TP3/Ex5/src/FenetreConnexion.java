import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class FenetreConnexion extends GridPane{
    private Button bouton;

    public FenetreConnexion(Button b){
        super();
        this.bouton = b;
        
        Label titre = new Label("Entrez votre nom d'utilisateur et votre mot de passe");
        Label labelA = new Label("Nom d'Utilisateur");
        Label labelB = new Label("Mot De Passe");
        TextField TF1 = new TextField();
        TextField TF2 = new TextField();
        this.add(titre,0,0,3,1);
        this.add(labelA,0,1);
        this.add(labelB,0,2);
        this.add(this.bouton,1,3);
        this.add(TF1,1,1);
        this.add(TF2,1,2);
        GridPane.setHalignment(b,HPos.RIGHT);

        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(10,10,10,10));

    }
        // Construction du graphe de scène
    
    public void quitter(){
        Platform.exit();
    }
    
}
