import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.HPos;
// import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.Slider;
import javafx.scene.layout.GridPane;

public class FenetreConnexion extends Application{
    private TextField TF1;
    private TextField TF2;

    @Override
    public void start(Stage stage) throws Exception{
        // Construction du graphe de scène
        GridPane root = this.gridPane();
        
        Scene scene = new Scene(root);
        stage.setTitle("Allo45");
        stage.setScene(scene);
        stage.show();
    }
    @Override 
    public void init(){
        this.TF1 = new TextField();
        this.TF2 = new TextField();
    }
    public void quitter(){
        Platform.exit();
    }
    private GridPane gridPane(){
        GridPane pane = new GridPane();
        Label titre = new Label("Entrez votre nom d'utilisateur et votre mot de passe");
        Label labelA = new Label("Nom d'Utilisateur");
        Label labelB = new Label("Mot De Passe");
        Button b = new Button("Connexion");
        pane.add(titre,0,0,3,1);
        pane.add(labelA,0,1);
        pane.add(labelB,0,2);
        pane.add(b,1,3);
        pane.add(this.TF1,1,1);
        pane.add(this.TF2,1,2);
        GridPane.setHalignment(b,HPos.RIGHT);

        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(10,10,10,10));

        return pane;
    }
    public static void main(String [] args){
        launch(args);
    }
}
