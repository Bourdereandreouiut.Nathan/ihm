import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;


public class Fenetre3 extends VBox{
   
    private Button bouton;
    public Fenetre3(Button bouton){
        super();
        this.bouton = bouton;
        this.setSpacing(10);
        this.setPadding(new Insets(20));
        this.getChildren().add(this.bouton);
        this.getChildren().add(new Text("Tables de multiplication"));
        for (int i=0; i<10; i++){
            this.getChildren().add(new Text(i+" * 5 = " +(i*5)));
        }
        this.setStyle("-fx-background-color: #98fb98;");
        this.getChildren().add((new ImageView(new Image("https://publicdomainvectors.org/photos/Calculator-Icon.png"))));        
    }
    
    
    }