import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.*;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.ToggleGroup;
import java.util.List;
import java.util.Arrays;
import javafx.geometry.Pos;

public class AppliExo6 extends Application{
    @Override
    public void init(){

    }
    @Override 
    public void start(Stage stage) throws Exception{
        BorderPane root = new BorderPane();

        this.ajouteTopBorderPane(root);
        this.ajouteCenterGridPane(root);
        this.ajouteBotBorderPane(root);
        this.ajouteLeftVBox(root);

        Scene scene = new Scene(root);
        stage.setTitle("Exo 6");
        stage.setScene(scene);
        stage.show();
        
    }
    public void ajouteTopBorderPane(BorderPane root){
        BorderPane top = new BorderPane();
        Text txt = new Text("Consultation/Modification d'un joueur");
        txt.setFont(Font.font(" Arial ",FontWeight.NORMAL,28));
        top.setCenter(txt);
        root.setTop(top);
    }
    public void ajouteCenterGridPane(BorderPane root){
        GridPane center = new GridPane();
        center.setPadding(new Insets(10));
        Label lb1 = new Label("Pseudo : ");
        Label lb2 = new Label("Mot De Passe : ");
        Label lb3 = new Label("Poste : ");
        TextField TFlb1 = new TextField();
        TextField TFlb2 = new TextField();
        VBox vb = new VBox();
        ComboBox<String> cbs = new ComboBox<String>();
        cbs.getItems().addAll("Botlaner","Midlaner","Toplaner");
        RadioButton rb1 = new RadioButton("Débutant");
        RadioButton rb2 = new RadioButton("Intermédiaire");
        RadioButton rb3 = new RadioButton("Expert");
        ToggleGroup togGroup = new ToggleGroup();
        rb1.setToggleGroup(togGroup);
        rb2.setToggleGroup(togGroup);
        rb3.setToggleGroup(togGroup);
        vb.getChildren().addAll(rb1,rb2,rb3);
        vb.setPadding(new Insets(10));
        vb.setSpacing(10);
        TitledPane titp = new TitledPane("Niveau",vb);
        ImageView imge = new ImageView("Étudiant.jpg");
        imge.setFitHeight(125);
        imge.setFitWidth(125);

        center.add(lb1,0,0);
        center.add(lb2,0,1);
        center.add(lb3,0,2);
        center.add(TFlb1,1,0);
        center.add(TFlb2,1,1);
        center.add(imge,2,0,1,3);
        center.add(cbs,1,2,1,1);
        center.add(titp,1,3);
        center.setHgap(20);
        center.setVgap(20);
        
        //center.setBackground(new Background(new BackgroundFill(Color.GREEN,null,null)));
        center.setStyle("-fx-background-color:rgba(03, 100, 123,0.87);");
        root.setCenter(center);
    }
    public void ajouteLeftVBox(BorderPane root){
        VBox left = new VBox();
        left.setPadding(new Insets(10));
        Button b1 = new Button("Début");
        Button b2 = new Button("Précédent");
        Button b3 = new Button("Nouveau");
        Button b4 = new Button("Valider");
        Button b5 = new Button("Suivant");
        Button b6 = new Button("Dernier");
        List<Button> listb = Arrays.asList(b1,b2,b3,b4,b5,b6);
        for (Button b:listb){
            b.setMaxWidth(200);
        }
        left.getChildren().addAll(b1,b2,b3,b4,b5,b6);
        left.setAlignment(Pos.CENTER);
        left.setSpacing(10);
        left.setStyle("-fx-background-color:Beige;");
        root.setLeft(left);
    }
    public void ajouteBotBorderPane(BorderPane root){
        BorderPane bot = new BorderPane();
        bot.setPadding(new Insets(5));
        Label lb1 = new Label("nombre de fiches : 102");
        Label lb2 = new Label("Numéro de la fiche : 10");
        Label lb3 = new Label("Fiche modifiée*");
        bot.setLeft(lb1);
        bot.setRight(lb3);
        bot.setCenter(lb2);
        bot.setStyle("-fx-background-color:rgba(00, 255, 123,0.87);");
        root.setBottom(bot);
    }
    public static void main(String[] args){
        launch(AppliExo6.class,args);
    }
}
