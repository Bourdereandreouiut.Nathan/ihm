import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.layout.HBox;

public class ControleurClavier implements EventHandler<KeyEvent>{
    private AppliDessin appli;
    
    public ControleurClavier(AppliDessin appli){
        this.appli = appli;
    }
    
    public void handle(KeyEvent e){       
        if (e.getCode().equals(KeyCode.ADD)){
            this.appli.augmenteLeDernierCercle(); // https://docs.oracle.com/javafx/2/api/javafx/scene/input/KeyCode.html
            System.out.println("+");
        }
        else if(e.getCode().equals(KeyCode.SUBTRACT)){
            this.appli.diminueLeDernierCercle();
            System.out.println("-");
        }
        else if(e.getCode().equals(KeyCode.DELETE)){
                this.appli.effaceCercles();
                System.out.println("CLEAR");
            }
        else if(e.getCode().equals(KeyCode.MULTIPLY)){
            this.appli.changerCouleur();
            System.out.println("*");
        }
        }
    }

