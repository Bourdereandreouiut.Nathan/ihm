import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import java.util.*;
/**
 * Controleur du clavier
 */
public class ControleurLettres implements EventHandler<ActionEvent> {

    /**
     * modèle du jeu
     */
    private MotMystere modelePendu;
    /**
     * vue du jeu
     */
    private Pendu vuePendu;

    /**
     * @param modelePendu modèle du jeu
     * @param vuePendu vue du jeu
     */
    ControleurLettres(MotMystere modelePendu, Pendu vuePendu){
        this.modelePendu = modelePendu;
        this.vuePendu = vuePendu;
    }

    /**
     * Actions à effectuer lors du clic sur une touche du clavier
     * Il faut donc: Essayer la lettre, mettre à jour l'affichage et vérifier si la partie est finie
     * @param actionEvent l'événement
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        Button b = (Button)actionEvent.getTarget();
        Set<String> st = new HashSet<>();
        st.add(b.getText());
        this.vuePendu.getClavier().desactiveTouches(st);
        // for(int i=0;i<this.modelePendu.getMotATrouve().length();i++){
        //     if (this.modelePendu.getMotATrouve().charAt(i) == b.getText().charAt(0)){
        //         this.modelePendu.getMotCrypte().replace("*",b.getText());
        //     }
        // }
        this.modelePendu.essaiLettre(b.getText().charAt(0));
        this.vuePendu.majAffichage();
    }
}
