import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import java.util.Optional;
/**
 * Controleur des radio boutons gérant le niveau
 */
public class ControleurNiveau implements EventHandler<ActionEvent> {

    /**
     * modèle du jeu
     */
    private MotMystere modelePendu;


    /**
     * @param modelePendu modèle du jeu
     */
    public ControleurNiveau(MotMystere modelePendu) {
        this.modelePendu = modelePendu;
    }

    /**
     * gère le changement de niveau
     * @param actionEvent
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        String txtRadioB = " ";
        if (actionEvent.getTarget() instanceof RadioButton){
        RadioButton radiobouton = (RadioButton) actionEvent.getTarget();
        txtRadioB = radiobouton.getText();
        }
        switch(txtRadioB){
            // case default: //je dois chercher pour mettre deux contrôleurs sur le bouton lancer la partie...
            // Alert alert = new Alert(AlertType.INFORMATION,"",ButtonType.YES, ButtonType.NO);
            // alert.setTitle("ATTENTION");
            // alert.setHeaderText("Vous n'avez pas sélectionné de niveau");
            // alert.setContentText("Le niveau Facile sera sélectionné par défaut");
            // Optional<ButtonType> reponse = alert.showAndWait(); // on lance la fenêtre popup et on attends la réponse
            // // si la réponse est oui
            // if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
            //     this.modelePendu.setNiveau(MotMystere.FACILE);
            // }
            //break;
            case "Facile":
            this.modelePendu.setNiveau(MotMystere.FACILE);
            break;
            case "Normal":
            this.modelePendu.setNiveau(MotMystere.MOYEN);
            break;
            case "Difficile": 
            this.modelePendu.setNiveau(MotMystere.DIFFICILE);
            break;
            case "Expert": 
            this.modelePendu.setNiveau(MotMystere.EXPERT);
            break;
        }
    }
}
