import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import javafx.scene.shape.Circle ;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Génère la vue d'un clavier et associe le contrôleur aux touches
 * le choix ici est d'un faire un héritié d'un TilePane
 */
public class Clavier extends TilePane{
    /**
     * il est conseillé de stocker les touches dans une ArrayList
     */
    private List<Button> clavier;

    /**
     * constructeur du clavier
     * @param touches une chaine de caractères qui contient les lettres à mettre sur les touches
     * @param actionTouches le contrôleur des touches
     * @param tailleLigne nombre de touches par ligne
     */
    public Clavier(String touches, EventHandler<ActionEvent> actionTouches) {
        this.setPadding(new Insets(10));
        this.setHgap(30);
        this.setVgap(30);
        //this.setPrefTileWidth(8);
        this.clavier = new ArrayList<>();
        for (int i=0;i<touches.length();i++){
            Button b = new Button(String.valueOf(touches.charAt(i)));
            b.setMinWidth(50);
            b.setMinHeight(50);
            b.setShape(new Circle(1.5));
            b.setOnAction(actionTouches);
            this.clavier.add(b);
        }
        this.getChildren().addAll(this.clavier);
    }
    /**
     * permet de désactiver certaines touches du clavier (et active les autres)
     * @param touchesDesactivees une chaine de caractères contenant la liste des touches désactivées
     */
    public void desactiveTouches(Set<String> touchesDesactivees){
        try{
        for(String touche:touchesDesactivees){
            System.out.println(touchesDesactivees);
            touche.replace(" ", "");
            System.out.println(touchesDesactivees);
            for (Button b:this.clavier){
            if (b.getText().equals(touche))
            this.clavier.get(this.clavier.indexOf(b)).setDisable(true);
        }
        }
    }
        catch(IndexOutOfBoundsException ie){
            System.out.println(ie+" "+ie.getMessage());
        }
    }
    public void reactiveTouches(){
            for (Button b: this.clavier){
                if (this.clavier.get(this.clavier.indexOf(b)).isDisable()){
                this.clavier.get(this.clavier.indexOf(b)).setDisable(false);
            } 
        }
    }
}
