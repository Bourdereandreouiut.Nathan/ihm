import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TitledPane;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType ;
import java.util.*;
import java.io.File;
import java.math.*;


/**
 * Vue du jeu du pendu
 */
public class Pendu extends Application {
    /**
     * modèle du jeu
     **/
    private MotMystere modelePendu;
    /**
     * Liste qui contient les images du jeu
     */
    private ArrayList<Image> lesImages;
    /**
     * Liste qui contient les noms des niveaux
     */    
    public List<String> niveaux;

    // les différents contrôles qui seront mis à jour ou consultés pour l'affichage
    /**
     * le dessin du pendu
     */
    private ImageView dessin;
    /**
     * le mot à trouver avec les lettres déjà trouvé
     */
    private Text motCrypte;
    /**
     * la barre de progression qui indique le nombre de tentatives
     */
    private ProgressBar pg;
    /**
     * le clavier qui sera géré par une classe à implémenter
     */
    private Clavier clavier;
    /**
     * le text qui indique le niveau de difficulté
     */
    private Text leNiveau;
    /**
     * le chronomètre qui sera géré par une clasee à implémenter
     */
    private Chronometre chrono;
    /**
     * le panel Central qui pourra être modifié selon le mode (accueil ou jeu)
     */
    private BorderPane panelCentral;
    /**
     * le bouton Paramètre / Engrenage
     */
    private Button boutonParametres;
    /**
     * le bouton Accueil / Maison
     */    
    private Button boutonMaison;
    /**
     * le bouton qui permet de (lancer ou relancer une partie
     */ 
    private Button bJouer;

    /**
     * initialise les attributs (créer le modèle, charge les images, crée le chrono ...)
     */
    @Override
    public void init() {
        Tooltip tpParam = new Tooltip("fenêtre des paramètres");
        Tooltip tpHome = new Tooltip("Pour revenir à la fenêtre d'accueil, accessible qu'en jeu");
        this.niveaux = Arrays.asList("NIVEAU : Facile","NIVEAU : Normal","NIVEAU : Difficile","NIVEAU : Expert");
        this.leNiveau = new Text(this.niveaux.get(0));
        //this.modelePendu = new MotMystere("C:\\Users\\froma\\OneDrive\\Documents\\liste_francais.txt", 3, 10, MotMystere.FACILE, 10);
        this.modelePendu = new MotMystere("/usr/share/dict/words", 3, 10, MotMystere.FACILE, 10);
        this.lesImages = new ArrayList<Image>();
        this.chargerImages("./img");
        this.bJouer = new Button("Lancer la partie");
        this.bJouer.setOnAction(new ControleurLancerPartie(this.modelePendu, this));
        ImageView home = new ImageView("home.png");
        home.setFitHeight(30);
        //home.setFitWidth(25);
        home.setPreserveRatio(true);
        this.boutonMaison = new Button("",home);
        this.boutonMaison.setOnAction(new RetourAccueil(this.modelePendu, this));
        this.boutonMaison.setTooltip(tpHome);
        //this.boutonMaison.setDisable(true);
        ImageView param = new ImageView("parametres.png");
        //param.setFitWidth(25);
        param.setFitHeight(30);
        param.setPreserveRatio(true);
        this.boutonParametres = new Button("",param);
        this.boutonParametres.setTooltip(tpParam);
        this.boutonParametres.setOnAction(new ControleurParametres(this));
        this.motCrypte = new Text(this.modelePendu.getMotCrypte());
        this.motCrypte.setFont(Font.font(" Arial ",FontWeight.NORMAL,28));
        this.pg = new ProgressBar(0);
        this.dessin = new ImageView(this.lesImages.get(0));
        this.dessin.setFitHeight(500);
        this.dessin.setPreserveRatio(true);
        this.chrono = new Chronometre();
        this.clavier = new Clavier("ABCDEFGHIJKLMNOPQRSTUVWXYZ", new ControleurLettres(this.modelePendu, this));
    }


    /**
     * @return  le graphe de scène de la vue à partir de methodes précédentes
     */
    private Scene laScene(){
        BorderPane fenetre = new BorderPane();
        fenetre.setTop(this.titre());
        fenetre.setCenter(this.fenetreAccueil());
        this.panelCentral = fenetre;
        return new Scene(fenetre, 800, 1000);
    }

    /**
     * @return le panel contenant le titre du jeu
     */
    private BorderPane titre(){
        BorderPane banniere = new BorderPane();
        Text titreBanniere = new Text("Jeu Du Pendu");
        titreBanniere.setFont(Font.font(" Arial ",FontWeight.NORMAL,28));
        banniere.setPadding(new Insets(10));
        HBox hb = new HBox();
        //hb.setPadding(new Insets(10));
        ImageView help = new ImageView("info.png");
        help.setFitHeight(30);
        help.setFitWidth(30);
        Tooltip tpHelp = new Tooltip("Affiche le bouton");
        Button bhelp = new Button("",help);
        bhelp.setTooltip(tpHelp);
        bhelp.setOnAction(new ControleurInfos(this));
        hb.getChildren().addAll(this.boutonMaison,this.boutonParametres,bhelp);
        banniere.setStyle("-fx-background-color:rgba(03, 100, 123,0.87);");

        banniere.setLeft(titreBanniere);  
        banniere.setRight(hb);
        return banniere;
    }

     /**
      * @return le panel du chronomètre
      */
     private TitledPane leChrono(){
        
        TitledPane res = new TitledPane("Chronomètre",this.chrono);
        return res;
     }
     /**
      * 
      * @return récupère le bouton home sur le layout titre de l'appli pendu
      */
     public Button getBoutonHome(){
        return this.boutonMaison;
     }
     /**
      * 
      * @return récupère le bouton Paramètre sur le layout titre de l'appli pendu
      */
     public Button getBoutonParam(){
        return this.boutonParametres;
     }

     /**
      * @return la fenêtre de jeu avec le mot crypté, l'image, la barre
      *         de progression et le clavier
      */
     private BorderPane fenetreJeu(){
        BorderPane res = new BorderPane();
        GridPane right = new GridPane();
        VBox center = new VBox();
        center.setSpacing(15);
        center.setPadding(new Insets(10));
        right.setVgap(20);
        Button nouveauMot = new Button("Nouveau Mot");
        nouveauMot.setOnAction(new ControleurLancerPartie(this.modelePendu,this));
        right.add(this.leNiveau,0,0);
        right.add(leChrono(),0,1);
        right.add(nouveauMot,0,2);
        right.setPadding(new Insets(10));
        center.getChildren().addAll(this.motCrypte,this.dessin,this.pg,this.clavier);
        res.setCenter(center);
        res.setRight(right);
        return res;
     }
     /**
      * 
      * @return la fenêtre paramètres,
      * experimental (elle servira a changer la couleur de 
      *fond de la fenêtre de titre en cliquant sur un des boutons avec getStyle() )
      */
     private BorderPane fenetreParametre(){
        BorderPane bp = new BorderPane();
        HBox lesBoutons = new HBox();
        lesBoutons.setSpacing(15);
        Button b;
        lesBoutons.setPadding(new Insets(10));
        List<Button> listeButton = new ArrayList<>();
        for (int i=0;i<10;i++){
            b = new Button();
            b.setStyle("-fx-background-color:rgba("+i*10+", "+(i+2)*10+", "+i*15+",1);");
            listeButton.add(b);
        }
        for (Button bouton:listeButton){
            lesBoutons.getChildren().add(bouton);
        }
        bp.setCenter(lesBoutons);
        bp.setPadding(new Insets(10));
        return bp;
     }

     /**
      * @return la fenêtre d'accueil sur laquelle on peut choisir les paramètres de jeu
      */
     private VBox fenetreAccueil(){
        VBox res = new VBox();
        VBox vb = new VBox();
        ToggleGroup tg = new ToggleGroup();
        RadioButton rbfacile = new RadioButton("Facile");
        RadioButton rbnormal = new RadioButton("Normal");
        RadioButton rbdifficile = new RadioButton("Difficile");
        RadioButton rbexpert = new RadioButton("Expert");

        ControleurNiveau cn = new ControleurNiveau(this.modelePendu);
        rbfacile.setOnAction(cn);
        rbnormal.setOnAction(cn);
        rbdifficile.setOnAction(cn);
        rbexpert.setOnAction(cn);

        rbfacile.setToggleGroup(tg);
        rbnormal.setToggleGroup(tg);
        rbdifficile.setToggleGroup(tg);
        rbexpert.setToggleGroup(tg);

        vb.getChildren().addAll(rbfacile,rbnormal,rbdifficile,rbexpert);
        vb.setPadding(new Insets(10));
        vb.setSpacing(3);

        TitledPane difficulte = new TitledPane("Niveaux de difficultés",vb);
        res.getChildren().addAll(this.bJouer,difficulte);
        res.setSpacing(10);
        res.setPadding(new Insets(10));
        
        return res;
     }

    /**
     * charge les images à afficher en fonction des erreurs
     * @param repertoire répertoire où se trouvent les images
     */
    private void chargerImages(String repertoire){
        for (int i=0; i<this.modelePendu.getNbErreursMax()+1; i++){
            File file = new File(repertoire+"/pendu"+i+".png");
            //System.out.println(file.toURI().toString());
            this.lesImages.add(new Image(file.toURI().toString()));
        }
    }
     /** mets l'application sur la fenêtre d'accueil */
    public void modeAccueil(){
        this.panelCentral.setCenter(this.fenetreAccueil());
        this.boutonMaison.setDisable(true);
    }
     /** mets l'application sur la fenêtre jeu */
    public void modeJeu(){
        this.panelCentral.setCenter(this.fenetreJeu());
    }
     /** affiche les paramètres sous forme de fenêtre tout en laissant la fenêtre principale ouverte */
    public void modeParametres(){
        this.panelCentral.setCenter(this.fenetreParametre());
    }

    /** lance une partie */
    public void lancePartie(){
        this.leNiveau.setText(this.niveaux.get(this.modelePendu.getNiveau()));
        this.dessin.setImage(this.lesImages.get(0));
        this.modelePendu.setMotATrouver();
        this.clavier.reactiveTouches();
        this.motCrypte = new Text(this.modelePendu.getMotCrypte());
        this.clavier.desactiveTouches(this.modelePendu.getLettresEssayees());
        this.chrono.resetTime();
        this.chrono.start();
        if(this.boutonMaison.isDisable()){
            this.boutonMaison.setDisable(false);
        }
        this.boutonParametres.setDisable(true);
        this.majProgress();
        this.modeJeu();
    }
    /**
     * 
     * @return récupère la fenêtre centrale actuelle de l'appli pendu
     */
    public Pane getFenetre(){
        return this.panelCentral;
    }
    /**
     * raffraichit l'affichage selon les données du modèle
     */
    public void majProgress(){
            double valProgress = (double)this.modelePendu.getNbLettresRestantes()/(double)this.modelePendu.getMotATrouve().length();
            this.pg.setProgress(1-valProgress);
    }
    public void majImage(){
        try{
            this.dessin.setImage(this.lesImages.get(this.modelePendu.getNbErreursMax()-this.modelePendu.getNbErreursRestants()));
            }
            catch(IndexOutOfBoundsException ie){
            }
    }
    public void majAffichage(){
        this.motCrypte.setText(this.modelePendu.getMotCrypte());
        this.majProgress();
        this.clavier.desactiveTouches(this.modelePendu.getLettresDuMotCrypte());
        this.majImage();
        if (this.modelePendu.perdu()){
            this.chrono.stop();
            this.popUpMessagePerdu();
            Optional<ButtonType> option = this.popUpMessagePerdu().showAndWait();
             if(option.get() == ButtonType.OK){
                 this.lancePartie();
             }
             else{
                this.boutonMaison.setDisable(false);
            }
        }
        else if (this.modelePendu.gagne()){
            this.chrono.stop();
            Optional<ButtonType> option = this.popUpMessageGagne().showAndWait();
             if(option.get() == ButtonType.OK){
                 this.lancePartie();
             }
             else{
                this.boutonMaison.setDisable(false);
            }
        }
    }

    /**
     * accesseur du chronomètre (pour les controleur du jeu)
     * @return le chronomètre du jeu
     */
    public Chronometre getChrono(){
        return this.chrono;
    }
    public Clavier getClavier(){
        return this.clavier;
    }
    public Alert popUpPartieEnCours(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"La partie est en cours!\n Etes-vous sûr de l'interrompre ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }
        
    public Alert popUpReglesDuJeu(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Règles du jeu du pendu");
        alert.setHeaderText("Le but du jeu est de trouver un mot crypté et lettre par lettre avant que le dessin du pendu ne se complète");
        alert.setContentText("Vous pouvez vous tromper 10 fois et vous perdez si vous ne trouvez pas le mot avant la fin du temps imparti ou vous faites 10 erreurs");
        return alert;
    }
    
    public Alert popUpMessageGagne(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"",ButtonType.YES,ButtonType.NO);
        alert.setTitle("Victoire !!"); 
        alert.setHeaderText("Vous avez gagné, le mot était bien : " + this.modelePendu.getMotATrouve());
        alert.setContentText("Recommencer une nouvelle partie ?");       
        return alert;
    }
    
    public Alert popUpMessagePerdu(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Partie Perdue");
        alert.setHeaderText("Vous avez perdu, le mot était : " + this.modelePendu.getMotATrouve());
        alert.setContentText("Recommencer une nouvelle partie ?");
        return alert;
    }

    /**
     * créer le graphe de scène et lance le jeu
     * @param stage la fenêtre principale
     */
    @Override
    public void start(Stage stage) {
        stage.setTitle("IUTEAM'S - La plateforme de jeux de l'IUTO");
        stage.setScene(this.laScene());
        this.modeAccueil();
        stage.show();
    }

    /**
     * Programme principal
     * @param args inutilisé
     */
    public static void main(String[] args) {
        launch(args);
    }    
}
