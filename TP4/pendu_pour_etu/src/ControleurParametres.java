import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
/**
 * Contrôleur à activer lorsque l'on clique sur le bouton info
 */
public class ControleurParametres implements EventHandler<ActionEvent> {

    private Pendu appliPendu;

    /**
     * @param p vue du jeu
     */
    public ControleurParametres(Pendu appliPendu) {
        this.appliPendu = appliPendu;
    }

    /**
     * L'action consiste à afficher une fenêtre popup précisant les règles du jeu.
     * @param actionEvent l'événement action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        Button b = (Button)actionEvent.getTarget();
        if (b.equals(this.appliPendu.getBoutonParam())){
            this.appliPendu.getBoutonHome().setDisable(false);
            this.appliPendu.getBoutonParam().setDisable(false);
            this.appliPendu.modeParametres();
        }
    }
}
